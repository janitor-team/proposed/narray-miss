# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "narray_miss/version"

Gem::Specification.new do |s|
  s.name        = "narray_miss"
  s.version     = NArrayMiss::VERSION
  s.authors     = ["Seiya Nishizawa"]
  s.email       = ["seiya@gfd-dennou.org"]
  s.homepage    = "http://ruby.gfd-dennou.org/products/narray_miss/"
  s.summary     = %q{Additional class with processing of missing value to NArray}
  s.description = %q{NArrayMiss is an additional class with processing of missing value to NArray which is a numeric multi-dimensional array class.}

  s.rubyforge_project = "narray_miss"

  s.files         = [
    "Gemfile",
    "LICENSE.txt",
    "README.rdoc",
    "Rakefile",
    "lib/narray_miss.rb",
    "lib/narray_miss/narray_miss.rb",
    "lib/narray_miss/version.rb",
    "narray_miss.gemspec",
    "test/test_narray_miss.rb"
  ]
  s.test_files    = [ "test/test_narray_miss.rb" ]
  s.executables   = []
  s.require_paths = ["lib"]
  #s.extensions << "ext/extconf.rb"

  # specify any dependencies here; for example:
  #s.add_development_dependency "rspec"
  s.add_runtime_dependency "narray"
  #s.add_runtime_dependency "numru-narray"
end
